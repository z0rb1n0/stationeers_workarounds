#!/usr/bin/python3 -uB

import sys, os, argparse, time, fcntl, xml, copy

import xml.etree.ElementTree as ET

SAVE_FILE_REL = "world.xml"
META_FILE_REL = "world_meta.xml"



# fake logging placeholder
def log_message(message):
	for m in message.split(sep = "\n"):
		sys.stderr.write("%s\n" % m.strip())
	return 0


def main():


	ap = argparse.ArgumentParser(
		description = "\n".join(map(str.strip, str("""
			Injects a dummy atmosphere in every completed frame that
			does not already have one in an attempt to work around the
			heat transfer between pipes and the world atmosphere that
			frames contain by default.
			
			Untested on Windows, please provide feedback.

		""").split("\n"))),
		formatter_class = argparse.ArgumentDefaultsHelpFormatter
	)



	#ap.add_argument("command_line", nargs = "*", help = "Example all-capturing positional argument")
	#actions_group = ap.add_mutually_exclusive_group()
	#actions_group.add_argument("--yes", dest = "yes", action = "store_true", required = False, default = argparse.SUPPRESS, help = "Do yes example")
	#actions_group.add_argument("--no", dest = "no", action = "store_true", required = False, default = argparse.SUPPRESS, help = "Do no example")
	#ap.add_argument("--required-str", dest = "required_str", metavar = "EXAMPLE_STR", type = str, required = True, help = "Example of required argument")
	#ap.add_argument("--optional-int", dest = "optional_int", metavar = "EXAMPLE_INT", type = int, required = False, default = 0, help = "Example of optional argument")

	ap.add_argument("save_dir",
			help = "Stationeers save directory"
	)

	ap.add_argument("--backup-dir",
		dest = "backup_dir",
		metavar = "PATH",
		type = str,
		required = False,
		default = None,
		help = "Backup directory. Default is the save directory itself"
	)

	ap.add_argument("--no-backup",
		dest = "no_backup",
		action = "store_true",
		required = False,
		default = argparse.SUPPRESS,
		help = "Do not perform a backup of the save (dangerous)"
	)

	cmd_args = ap.parse_args()

	backup_dir = cmd_args.backup_dir
	if (backup_dir is None):
		backup_dir = os.path.dirname(cmd_args.save_dir)
	
	backup_suffix = "%s_%d" % (
		time.strftime("%Y%m%dT%H%M%S", time.gmtime()),
		os.getpid()
	)

	# there are multiple files at play...
	xml_types = {
		"save": {
			"orig": "%s/%s" % (cmd_args.save_dir, SAVE_FILE_REL),
			"backup": None,
			"fh": None,
			"raw": None,
			"tree": None
		},
		"meta": {
			"orig": "%s/%s" % (cmd_args.save_dir, META_FILE_REL),
			"backup": None,
			"fh": None,
			"raw": None,
			"tree": None
		}
	}

	for xml_type in xml_types:
		xml_types[xml_type]["backup"] = "%s_%s" % (
			xml_types[xml_type]["orig"],
			backup_suffix
		)

	# We're doing each file read twice 'cause the XML library can
	# only load strings as elements, not whole trees
	for x_type in sorted(xml_types):

		x_info = xml_types[x_type]

		log_message("Locking and loading `%s`..." % x_info["orig"])
		try:

			xml_types[x_type]["fh"] = open(x_info["orig"], "rb")
			fcntl.flock(xml_types[x_type]["fh"], fcntl.LOCK_EX)
			# we're not stingy with memory, it's 2020
			# and the process is short-lived
			xml_types[x_type]["raw"] = xml_types[x_type]["fh"].read()
		
		except (
			OSError, FileNotFoundError,
			PermissionError, xml.etree.ElementTree.ParseError
		) as e_noload:
			log_message("Unable to load save XML at `%s`: %s(%s)" % (
				x_info["orig"], e_noload.__class__.__name__, e_noload
			))
			return 4

		log_message("Decoding XML...")
		try:
			xml_types[x_type]["tree"] = ET.parse(x_info["orig"])
		except (xml.etree.ElementTree.ParseError) as e_xml:
			log_message("Could not decode XML: %s(%s)" % (
				e_xml.__class__.__name__, e_xml
			))


	# shorthand
	save_tree = xml_types["save"]["tree"]

	# ElementTree does not seem to support boolean operators in XPath
	log_message("Retrieving frame coordinates")
	frames = set()
	for frame in (
			save_tree.findall("./Things/ThingSaveData/[PrefabName='StructureFrameIron']")
		+
			save_tree.findall("./Things/ThingSaveData/[PrefabName='StructureFrame']")
	):
		if (frame.find("./CurrentBuildState").text != "2"):
			continue
		f_pos = frame.find("./WorldPosition")
		frames.add(tuple(
			int(round(float(f_pos.find("./%s" % axis).text))) for axis in [
				"x", "y", "z"
			]
		))

	log_message("Total fully built unique frames: %d" % len(frames))
	#print(frames)


	# there ARE duplicate atmosphere coordinates, but we do not care
	log_message("Retrieving matching atmospheres")
	atmospheres = set()
	total_atmos = 0 # we still need to count them to change the meta file
	for atmo_pos in save_tree.findall("./Atmospheres/AtmosphereSaveData/Position"):
		total_atmos += 1
		atmospheres.add(tuple(
			int(round(float(atmo_pos.find("./%s" % axis).text))) for axis in [
				"x", "y", "z"
			]
		))

	log_message("Total atmospheres: %d" % len(atmospheres))
	#print(atmospheres)

	made_changes = False
	if (any(frames) and any(atmospheres)):

		missing_atmos = frames.difference(atmospheres)
		total_atmos += len(missing_atmos)

		if (any(missing_atmos)):

			made_changes = True
			log_message("Adding %d atmosphere to frames..." % len(missing_atmos))

			if ("no_backup" in cmd_args):
				log_message("Creation of backup files is recklessly suppressed")
			else:
				log_message("Creating backup copies:")

				for (x_type, x_info) in xml_types.items():
					log_message("..`%s` -> `%s`" % (x_info["orig"], x_info["backup"]))
					
					try:
						bfh = open(x_info["backup"], "wb")
						bfh.write(x_info["raw"])
						bfh.flush()
						bfh.close()
					except (OSError, FileNotFoundError,	PermissionError) as e_backup:
						log_message("Unable create backup file `%s`: %s(%s)" % (
							x_info["backup"], e_noload.__class__.__name__, e_noload
						))
						return 4


			atmos_elem = save_tree.find("./Atmospheres")
			# We copy the first atmosphere so that if tags are added in future versions
			# we don't need to worry about adapting the code
			template = atmos_elem[0]

			for new_atmo in sorted(missing_atmos):

				#log_message("...%s" % str(new_atmo))
				next_atmo = copy.deepcopy(template)
				
				# need to work out how to better do this
				pos = next_atmo.find("./Position")

				for (tup_idx, axis) in enumerate(["x", "y", "z"]):
					pos.find("./%s" % axis).text = "%d" % new_atmo[tup_idx]

				dir = next_atmo.find("./Direction")
				for axis in ["x", "y", "z"]:
					dir.find("./%s" % axis).text = "0"

				for atmo_att in next_atmo:
					if (atmo_att.tag in [
						"Oxygen", "Nitrogen",
						"CarbonDioxide", "Volatiles",
						"Chlorine", "Water",
						"NitrousOxide",
						"Energy",
						"ThingReferenceId",
						"CleanBurnRate",
						"NetworkReferenceId",
						"MothershipReferenceId",
					]):
						atmo_att.text = "0"

				next_atmo.find("./Volume").text = "8000"

				atmos_elem.append(next_atmo)

			log_message("Updating the number of Atmospheres in `%s`" % (
				xml_types["meta"]["orig"]
			))
			
			xml_types["meta"]["tree"].find("./NumberOfAtmospheres").text = "%d" % total_atmos
			

		else:
			log_message("All frames seem to already have an atmosphere")

	else:
		log_message("Cannot proceeed, there must be both frames and atmospheres in the map!")

	#print(atmospheres)
	for (x_type, x_info) in xml_types.items():

		if (made_changes):
			log_message("Saving file `%s`" % x_info["orig"])
			x_info["tree"].write(x_info["orig"])


		#print(xml.etree.ElementTree.tostring(save_tree.getroot()).decode())
		
		log_message("Releasing `%s`" % x_info["orig"])

		fcntl.flock(x_info["fh"], fcntl.LOCK_UN)
		x_info["fh"].close()



	return 0


if (__name__ == "__main__"):
	sys.exit(main())


