#!/usr/bin/python3 -uB

import sys, os, xml

import xml.etree.ElementTree as ET

XML_SAVE = "/solid/home/z0rb1n0/backups/stationeers/saves/before_death_poke/world.xml"

def _lift_node_branch(node, path):
	tag = node.tag
	full_path = "%s/%s" % (path, tag)
	print(full_path)
	if (tag == "y"):
		print(node.text)
	#position = atmosphere.find("ThingReferenceId")
	#print(position):
	for child in node:
		_lift_node_branch(node = child, path = full_path)

def main():
	tree = ET.parse(XML_SAVE)

	root = tree.getroot()
	_lift_node_branch(root, "")

# 	for element in root:
# 		if (element.tag != "Atmospheres"):
# 			continue
# 		for atmosphere in element:
# 			if (atmosphere.find("ThingReferenceId").text != "0"):
# 				continue
# 			for atmo_att in atmosphere:
# 				if (atmo_att.tag in [
# 					"Oxygen", "Nitrogen",
# 					"CarbonDioxide", "Volatiles",
# 					"Chlorine", "Water",
# 					"NitrousOxide",

# 					"Energy"
# 				]):
# 					atmo_att.text = "0"


# 	tree.write(XML_SAVE)

	return 0







if (__name__ == "__main__"):
	sys.exit(main())
