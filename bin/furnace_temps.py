#!/usr/bin/python3 -uB
import sys, os


class Vector2(tuple):

	def __new__(cls, *args):

		if (len(args) == 2):
			axes = args
		elif ((len(args) == 1) and isinstance(args[0], (tuple, list)) and (len(args[0]) == 2)):
			axes = args[0]
		else:
			raise ValueError("Vector2 definition expects two arguments or a 2-tuple")

		# validation
		for (axis, axis_id) in [("x", 0), ("y", 1)]:
			if (not isinstance(axes[axis_id], (int, float))):
				raise TypeError("Type `%s` cannot be cast to float (value `%s` was passed to `%s` axis of point" % (
					axes[axis_id].__class__.__name__, axes[axis_id], axis
				))

		return super().__new__(cls, [float(a) for a in axes])

	def _x_get(self):
		return self[0]
	x = property(fget = _x_get, doc = "The X coordinate")

	def _y_get(self):
		return self[1]
	y = property(fget = _y_get, doc = "The Y coordinate")

	def _magnitude_get(self):
		return (self.x ** 2.0 + self.y ** 2.0) ** 0.5
	magnitude = property(fget = _magnitude_get, doc = "The magnitude")
	m = magnitude

	def __add__(self, other):
		oth = self.__class__(other)
		return self.__class__([(self[dim] + oth[dim]) for dim in range(len(self))])

	def __sub__(self, other):
		oth = self.__class__(other)
		return self.__class__([(self[dim] - oth[dim]) for dim in range(len(self))])

	def __str__(self):
		return "x=%f,y=%f" % (self[0], self[1])

class Box(tuple):

	def __new__(cls, *args):

		if (len(args) == 4):
			# accept four individual coordinates
			points = (args[0:2], args[2:4])
		elif (len(args) == 2):
			points = args
		elif ((len(args) == 1) and isinstance(args[0], (tuple, list)) and (len(args[0]) == 2)):
			points = args[0]
		else:
			raise ValueError("A box requires either 4 arguments (x1, y1, x2, y2), two point-castable ones or a 2-tuple of points")

		# upper right, then lower left
		(p1, p2) = (Vector2(*points[0]), Vector2(*points[1]))

		return super().__new__(cls, (
			Vector2(max(p1.x, p2.x), max(p1.y, p2.y)),
			Vector2(min(p1.x, p2.x), min(p1.y, p2.y))
		))


	def _top_right_get(self):
		return self[0]
	top_right = property(fget = _top_right_get, doc = "The top right corner")
	tr = top_right

	def _bottom_left_get(self):
		return self[1]
	bottom_left = property(fget = _bottom_left_get, doc = "The bottom left corner")
	bl = bottom_left

	def _center_get(self):
		return Vector2((self[0].x + self[1].x) * 0.5, (self[0].y + self[1].y) * 0.5)
	center = property(fget = _center_get, doc = "The center of the box")
	c = center

	def _area_get(self):
		return (self[0].x - self[1].x) * (self[0].y - self[1].y)
	area = property(fget = _area_get, doc = "The area of the box")

	def _hypotenuse_get(self):
		return 
	hypotenuse = property(fget = _hypotenuse_get, doc = "The hypotenuse of the box")
	hyp = hypotenuse


	def __str__(self):
		return "upper_right:%s,bottom_left:%s" % (self[0], self[1])

	def __and__(self, comp):
		""" Returns the overlap bounding box, if any """
		
		# for each dimension, it's
		# larges bottom-left + smallest top-right minus largest bottom-left
		other = Box(comp)

		p1 = []
		p2 = []
		for (idx, dim) in enumerate(["x", "y"]):

			base = max(getattr(self.bl, dim), getattr(other.bl, dim))
			size = min(getattr(self.tr, dim), getattr(other.tr, dim)) - base

			# no intersection?
			if (size <= 0):
				return None
				
			p1.append(base)
			p2.append(base + size)

		return Box(p1, p2)


# 4-tuple: (pressure-min, pressure-max, temp-min, temp-max
ALLOYS = {
	"STEEL": (100000, 100000000, 600, 99999),
	"ELEC": (800000, 2400000, 700, 10000),
	"INVAR": (6000000, 7000000, 1200, 2000),
	"CONST": (100000, 10000000, 1000, 1500),
	"SOLD": (300000, 3500000, 300, 2000),
	"ASTRO": (5000000, 6000000, 1200, 1400),
	"HASTE": (2500000, 3000000, 950, 1000),
	"INCON": (4250000, 4750000, 1150, 1250),
	"WASPA": (1250000, 2750000, 875, 1000),
	"STELLITE": (4000000, 5000000, 1700, 1900)
}
ASM_BLOCK_TEMPLATE = """
	bne t_regime {dial_setting} no_{dial_setting} # {alloys_cs}
	move target_p {kpa}
	move target_t {kelvin}
	no_{dial_setting}:
"""


def main():
	

	regimes = {}
	for (alloy, params) in ALLOYS.items():
		regimes[Box(params[0], params[2], params[1], params[3])] = {alloy}


	round_id = 0
	while (True):

		print("---")
		print("- ROUND %d" % round_id)
		print("---")

		comments_block = ""
		stack_block = ""

		stack_lines = [] # this is for pushing into the stack

		for (dial_setting, regime) in enumerate(
			iterable = sorted(regimes, key = lambda r : (-len(regimes[r]), r.center.y)),
			start = 1
		):
			# pack the value into a single number:
			# round_id(pressure) * 10000 + round_id(temperature)
			kpa = int(round(0.001 * regime.center.x))
			kelvin = int(round(regime.center.y))
			alloys_cs = ",".join(sorted(regimes[regime]))
			t_regime = "%dKPa,%dK" % (kpa, kelvin)
			comments_block += "# %d: %s: %s\n" % (dial_setting, t_regime, alloys_cs)

			if (max(kpa, kelvin) <= 9999):
				stack_str = "push %d # %d: %s" % (
					10000 * kpa + kelvin,
					dial_setting,
					alloys_cs
				)
			else:
				stack_str = "#!!!TOO_LARGE `%s`(%d, %d)" % (
					alloys_cs, kpa, kelvin
				)
			stack_lines.append(stack_str)

		stack_block = "define REGIME_MAX %d\n" % len(stack_lines)
		stack_block += "move sp 0\n"
		stack_block += "\n".join(pnt for pnt in stack_lines) + "\n"

		print(comments_block)
		print(stack_block)

		round_id += 1

		new_regimes = {}
		# it is important to sort by length of the set or supersets will be
		# destroyed and re-created ad infinitum
		for regime_a in sorted(regimes, key = lambda r : len(regimes[r])):

			alloys_a = regimes[regime_a]
			# The following will be eventually set to a 2-tuple
			# indicating the matched alloys and the regime overlap to theirs
			best_match = None

			has_overlaps = False
			for regime_b in sorted(regimes, key = lambda r : len(regimes[r])):
				alloys_b = regimes[regime_b]

				if (regime_b == regime_a):
					continue

				# we looking for the overlap with the lowest
				# distance from the origin
				overlap = regime_a & regime_b
				if (overlap is not None):
					#print(regime_a, alloys_a, regime_b, alloys_b)
					new_regimes[overlap] = alloys_a.union(alloys_b)
					has_overlaps = True

			if (has_overlaps):
				del(regimes[regime_a])

		if (not new_regimes):
			break

		regimes.update(new_regimes)
		#import pprint; pprint.pprint(new_regimes)

	return 0
	
	
if (__name__ == "__main__"):
	sys.exit(main())
